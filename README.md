# TextTwist Demo #

A code demo for generation of words in a game of TextTwist.

### WordList.cs ###

The WordList class is simply a reference list of all valid words. The list is obtained from the *Official Scrabble Players Dictionary*.

### JumbleSet.cs ###

The JumbleSet class is a set of words that can be formed from the permutations of a set of letters.

### TextTwistGame.cs ###

The TextTwistGame class uses words 3-7 letters long. From one 7-letter word, get all words that can be formed from the permutations of the 7-letter word.

### References ###

* [National Puzzlers' League wordlist](http://puzzlers.org/dokuwiki/doku.php?id=solving:wordlists:about:start)
* [Official Scrabble Players Dictionary](https://en.wikipedia.org/wiki/Official_Scrabble_Players_Dictionary)