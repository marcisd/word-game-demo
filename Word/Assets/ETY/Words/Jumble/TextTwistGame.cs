﻿using System.Collections.Generic;
using Random = System.Random;

namespace ETY.Words.Jumble {
	
	/// <summary>
	/// Sample TextTwist class. Gets a random 7-word letter and list all the words in it (minimum letter count 3).
	/// </summary>
	public class TextTwistGame {

		private static readonly int MIN_CHARS = 3;
		private static readonly int MAX_CHARS = 7;

		private static Random s_rng = null;

		private List<string> _maxCharacterLettersPool = null;
		private JumbleSet _currentSet = null;
		private WordList _wordList;

		public char[] letters {
			get { 
				return _currentSet.letters;
			}
		}

		public List<string> words {
			get { 
				return _currentSet.words;
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ETY.Words.Jumble.TextTwistGame"/> class.
		/// </summary>
		public TextTwistGame () {
			_wordList = new WordList (MIN_CHARS, MAX_CHARS);
			_maxCharacterLettersPool = _wordList.WordsWithLength (MAX_CHARS);

			if (s_rng == null) {
				s_rng = new Random ();
			}
		}

		/// <summary>
		/// Loads the new set of jumbled letters.
		/// </summary>
		public void LoadNewSet () {
			int random = s_rng.Next (_maxCharacterLettersPool.Count);
			string randomWord = _maxCharacterLettersPool [random];
			_currentSet = new JumbleSet (randomWord, _wordList);
		}
	}
}
