﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ETY.Words.Jumble {

	/// <summary>
	/// Contains a list of all valid words found in all permutations of a set of letters.
	/// </summary>
	public class JumbleSet {

		private bool _sorted = false;

		private WordList _wordList;
		private List<string> _words = null;

		/// <summary>
		/// Gets the list of words.
		/// </summary>
		/// <value>The words.</value>
		public List<string> words {
			get { 
				if (!_sorted) {
					_words.Sort ((a, b) => a.Length - b.Length);
				}
				return _words;
			}
		}

		/// <summary>
		/// The set of letters.
		/// </summary>
		public readonly char[] letters = null;

		/// <summary>
		/// Initializes a new instance of the <see cref="ETY.Words.Jumble.JumbleSet"/> class using the set of letters.
		/// </summary>
		/// <param name="letters">Letters.</param>
		/// <param name="wordList">Word List.</param>
		public JumbleSet (char[] letters, WordList wordList) {
			this.letters = letters;
			_wordList = wordList;
			_sorted = false;
			HashSet<string> searchedWords = new HashSet<string> ();
			SearchWords ("", letters, ref searchedWords);
			_words = searchedWords.ToList ();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ETY.Words.Jumble.JumbleSet"/> class using the letters in the string.
		/// </summary>
		/// <param name="letters">Letters.</param>
		/// <param name="wordList">Word List.</param>
		public JumbleSet (string letters, WordList wordList) : this (letters.ToCharArray (), wordList) {}

		private void SearchWords (string prefix, char[] letters, ref HashSet<string> searchedWords) {

			for (int i = 0; i < letters.Length; i++) {
				string newPrefix = prefix + letters [i];

				// logically, it might be better to check if there are any words in the dictionary starting with the prefix
				// but such operation performs poorly so just go through all permutations
				char[] remainingLetters = letters.RemoveAt (i);
				SearchWords (newPrefix, remainingLetters, ref searchedWords);
			}

			if (_wordList.ContainsWord (prefix)) {
				searchedWords.Add (prefix);
			}
		}
	}

	internal static class Extensions {

		/// <summary>
		/// Removes an item in array at index.
		/// </summary>
		/// <returns>The <see cref="``0[]"/>.</returns>
		/// <param name="source">Source.</param>
		/// <param name="index">Index.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] RemoveAt<T> (this T[] source, int index) {
			T[] dest = new T[source.Length - 1];

			if (index > 0) {
				Array.Copy (source, 0, dest, 0, index);
			}

			if (index < source.Length - 1) {
				Array.Copy (source, index + 1, dest, index, source.Length - index - 1);
			}

			return dest;
		}
	}
}
