﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ETY.Words {

	/// <summary>
	/// A complete list of words. See the Official Scrabble Players Dictionary (OSPD) for more info.
	/// </summary>
	public class WordList {

		private static readonly string OSPD_FILE_NAME = "ospd";

		private static HashSet<string> s_origWordList = null;

		private static void LoadOriginalWordList () {
			if (s_origWordList == null) {
				try {
					TextAsset ospd = (TextAsset)Resources.Load (OSPD_FILE_NAME, typeof (TextAsset));
					string ospdStr = ospd.text;
					string[] split = ospdStr.Split ('\n');
					s_origWordList = new HashSet<string> (split);
				} catch (System.Exception e) {
					Debug.LogError ("Failed loading word list asset with message:\n" + e.Message);
				}
			}
		}

		private HashSet<string> _currentList = null;

		/// <summary>
		/// Initializes a new instance of the <see cref="ETY.Words.WordList"/> class.
		/// </summary>
		public WordList () {
			LoadOriginalWordList ();
			_currentList = new HashSet<string> (s_origWordList);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ETY.Words.WordList"/> class 
		/// to only contain words with letter count not less than <c>minChar</c> and not more than <c>maxChar</c>.
		/// </summary>
		/// <param name="minChar">Minimum char.</param>
		/// <param name="maxChar">Max char.</param>
		public WordList (int minChar, int maxChar) {
			LoadOriginalWordList ();
			_currentList = new HashSet<string> (s_origWordList);
			_currentList.RemoveWhere (s => s.Length < minChar || s.Length > maxChar);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ETY.Words.WordList"/> class 
		/// to only contain words with letter count equal to <c>minChar</c> and above.
		/// </summary>
		/// <param name="minChar">Minimum char.</param>
		public WordList (int minChar) : this (minChar, int.MaxValue) {}

		/// <summary>
		/// Sets the word list to only contain words with letter count not less than <c>minChar</c> and not more than <c>maxChar</c>.
		/// </summary>
		/// <param name="minChar">Minimum letter count.</param>
		/// <param name="maxChar">Maximum letter count.</param>
		public void SetMinMax (int minChar, int maxChar) {
			_currentList = new HashSet<string> (s_origWordList);
			_currentList.RemoveWhere (s => s.Length < minChar || s.Length > maxChar);
		}

		/// <summary>
		/// Sets the word list to only contain words with letter count equal to <c>minChar</c> and above.
		/// </summary>
		/// <param name="minChar">Minimum letter count.</param>
		public void SetMinOnly (int minChar) {
			_currentList = new HashSet<string> (s_origWordList);
			_currentList.RemoveWhere (s => s.Length < minChar);
		}

		/// <summary>
		/// Sets the word list to only contain words with letter count equal to <c>maxChar</c> and below.
		/// </summary>
		/// <param name="maxChar">Maximum letter count.</param>
		public void SetMaxOnly (int maxChar) {
			_currentList = new HashSet<string> (s_origWordList);
			_currentList.RemoveWhere (s => s.Length > maxChar);
		}
			
		/// <summary>
		/// Checks if the word can be found in the list.
		/// </summary>
		/// <returns><c>true</c>, if the word can be found in the list, <c>false</c> otherwise.</returns>
		/// <param name="word">Word to check.</param>
		public bool ContainsWord (string word) {
			word = word.ToLower ();
			return _currentList.Contains (word);
		}

		/// <summary>
		/// Returns the list of words that start with the <c>prefix</c>. Caution: Performs poorly.
		/// </summary>
		/// <returns>List of words that start with the <c>prefix</c></returns>
		/// <param name="prefix">Prefix.</param>
		public List<string> WordsStartingWith (string prefix) {
			return new List<string> (_currentList.Where (s => s.StartsWith (prefix)));
		}

		/// <summary>
		/// Returns the list of words where the number of letters equal <c>length</c>. Caution: Performs poorly.
		/// </summary>
		/// <returns>List of words where the number of letters equal <c>length</c>.</returns>
		/// <param name="length">Length.</param>
		public List<string> WordsWithLength (int length) {
			return new List<string> (_currentList.Where (s => s.Length == length));
		}
	}
}
