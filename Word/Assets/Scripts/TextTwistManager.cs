﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ETY.Words.Jumble;
using UnityEngine.UI;

public class TextTwistManager : MonoBehaviour {

	[System.Serializable]
	public class Letters {
		public Text letter1;
		public Text letter2;
		public Text letter3;
		public Text letter4;
		public Text letter5;
		public Text letter6;
		public Text letter7;
	}

	[System.Serializable]
	public class WordsListContainer {
		public RectTransform container;
		public Text placeholder;
	}
		
	public Letters letters;
	public Button generateButton;
	public WordsListContainer wordsListContainer;

	private TextTwistGame _textTwistGame;

	// Use this for initialization
	void Start () {
		_textTwistGame = new TextTwistGame ();
		generateButton.onClick.AddListener (GenerateGame);
		GenerateGame ();
	}
	
	void GenerateGame () {

		if (!IsValid ())
			return;

		// load a new Text Twist Level
		_textTwistGame.LoadNewSet ();

		// displays the letters
		var chars = _textTwistGame.letters;
		letters.letter1.text = chars [0].ToString ();
		letters.letter2.text = chars [1].ToString ();
		letters.letter3.text = chars [2].ToString ();
		letters.letter4.text = chars [3].ToString ();
		letters.letter5.text = chars [4].ToString ();
		letters.letter6.text = chars [5].ToString ();
		letters.letter7.text = chars [6].ToString ();

		// delete contents of the container if any
		foreach (Transform obj in wordsListContainer.container.transform) {
			if (!obj.gameObject.Equals (wordsListContainer.placeholder.gameObject))
				GameObject.Destroy (obj.gameObject);
		}

		// displays all the words in the container
		var list = _textTwistGame.words;
		wordsListContainer.placeholder.gameObject.SetActive (true);
		list.ForEach ((item) => {
			var t = GameObject.Instantiate (wordsListContainer.placeholder);
			t.text = item;
			t.transform.SetParent (wordsListContainer.container, false);
		});
		wordsListContainer.placeholder.gameObject.SetActive (false);
	}

	bool IsValid () {
		return
			letters.letter1 != null &&
			letters.letter2 != null &&
			letters.letter3 != null &&
			letters.letter4 != null &&
			letters.letter5 != null &&
			letters.letter6 != null &&
			letters.letter7 != null &&
			generateButton != null &&
			wordsListContainer.container != null &&
			wordsListContainer.placeholder != null;
	}
}
